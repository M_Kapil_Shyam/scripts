#!/usr/bin/python3.8

from bs4 import BeautifulSoup
from bs4 import Comment
import codecs
import os
import re
import sys
from html.parser import HTMLParser

dir=input('Enter the path to walk: ')
for folders,subs,files in os.walk(dir):               #This will walk (iterate) through the path starting from the '/html' path and it is stored in the folders,sub folders, and the files
 for filename in files:                                    #This will iterate among the filenames present in the file
  with codecs.open(os.path.join(folders,filename),'r',encoding='utf-8',errors='ignore') as f:   # it will open the current file which is iterated
   if filename.endswith('source.html'):                 # this will ignore the filename ending with source.html
    pass
   elif filename.endswith('.html'):               # if the filename ends with .html, it will proceed
    if filename.startswith('dir_') | filename.startswith('globals_') | filename.startswith('md_') | filename.startswith('examples'):                 #If the file name starts with dir_, md_, globals_, examples, it will ignore the files
      pass
    else:
      soup=BeautifulSoup(f.read(),'html5lib')
      print ('----------------------\n'+filename+'\n----------------------------')         # Prints the filename
      lines= soup.select('div .memitem')                                 # The class memitem is searchd in the html file and stored in the variable 'lines'

      for line in lines:                                                  #This line is for iterating between the lines of the class memitem in html file

             c=line.select('.memdoc')                                     # the memdoc class in the html code is found here
             tag=line.find('td')                                          # the 'td' tag is searched in the current iterating line
             a=tag.contents                                               # 'a' contains the contents in the tag 'td' which contains the function name
             if c[0].select('p') :                                        # This line is to check if the 'p' tag is present in the html file
              sys.stdout=open("optd.txt",'a+')
              print ("The function",a[0],"is documented")                 #This line is for printing the functions which are documented
             else :
              sys.stdout=open("optnd.txt",'a+')
              print ("The",a[0],"function is not documented")              #This line is for printing the functions which are not documented

