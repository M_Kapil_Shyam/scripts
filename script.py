#!/usr/bin/python3.8

import codecs
import os
import re
import sys

dir = input("Enter the directory path: ")

for folders,subs,files in os.walk(dir):

  for filename in files:
    with codecs.open(os.path.join(folders,filename),'r',encoding='utf-8',errors='ignore') as f:
      fin=f.read()
      author_name=re.findall('Name of Author\s*: ([A-Za-z& ;]+)*(\S*)',fin)
      mail_id=re.findall("Email ID\s*:\s*(\S*)",fin)
      lic_name=re.search('MIT|BSD|GNU General Public License|Mozilla Public License|Apache License|GNU Library|Common Development and Distribution License|Eclipse Public License',fin)
      an=[''.join(i)  for i in author_name if len(author_name)>=1]


      if (filename=="Makefile") | (filename=="README.md") | (filename=='readme.md') | (filename=='Readme.md') | (filename=='.gitkeep') | (filename=='.git') | (filename == '.gitlab') | ( filename == '.gitmodules') | (filename=='.gitlab-ci.yml') :
        pass
      elif (folders == '/shakti-sdk/.git/') | (folders == '/shakti-sdk/.gitkeep/') | (folders == '/shakti-sdk/.gitlab/') :
        break
      elif (filename=="script.py") | (filename=="script.py.save") | (filename=="script.pyc"):
        pass
      else:
        if (len(author_name)!=0):
         b = an[0]
         name = an[0]
         names= name.split(' & ')
         n1 = names[0]
         n2 = 'NIL'
         if len(names)==2:
           n2 = names[1]
         sys.stdout=open("file_metadata.txt",'a+')
         if (lic_name!=None) & (((len(n1)!=0) & (len(n2)!=0)) | (len(an[0])!=0)) :
            if len(mail_id)!=0:
              try:
                 print (os.path.join(folders,filename)+','+lic_name.group(0)+','+n1+','+mail_id[0]+','+n2+',NIL')
              except AttributeError:
                 print (os.path.join(folders,filename)+','+lic_name+','+n1+','+mail_id[0]+','+n2+',NIL')
            elif len(mail_id)==0:
              try:
                 print (os.path.join(folders,filename)+','+lic_name.group(0)+","+n1+',NIL,'+n2+',NIL')
              except AttributeError:
                 print (os.path.join(folders,filename)+','+lic_name+","+n1+',NIL,'+n2+',NIL')

         elif (lic_name==None) & (((len(n1)!=0) & (len(n2)!=0)) | (len(an[0])!=0)) :
              if len(mail_id)!=0:
                print (os.path.join(folders,filename)+',NIL,'+n1+','+mail_id+','+n2+',NIL')
              elif len(mail_id)==0:
                print (os.path.join(folders,filename)+',NIL,'+n1+',NIL,'+n2+',NIL')
         elif (lic_name!=None) & (((len(n1)!=0) & (len(n2)==0)) | (len(an[0])!=0)) :
              if len(mail_id)!=0:
                try:
                   print (os.path.join(folders,filename)+','+lic_name.group(0)+','+an[0]+','+mail_id[0])
                except AttributeError:
                   print (os.path.join(folders,filename)+','+lic_name+','+an[0]+','+mail_id[0])
              elif len(mail_id)==0:
                try:
                   print (os.path.join(folders,filename)+','+lic_name.group(0)+","+an[0]+',NIL')
                except AttributeError:
                   print (os.path.join(folders,filename)+','+lic_name+","+an[0]+',NIL')

         elif (lic_name==None) & (((len(n1)!=0) & (len(n2)==0)) | (len(an[0])!=0)) :
              if len(mail_id)!=0:
                 print (os.path.join(folders,filename)+',NIL,'+an[0]+','+mail_id)
              elif len(mail_id)==0:
                 print (os.path.join(folders,filename)+',NIL,'+an[0]+',NIL')

        elif (lic_name==None) & (len(author_name)==0):
              if len(mail_id)!=0:
                try:
                  print (os.path.join(folders,filename)+",NIL,NIL,"+mail_id[0])
                except AttributeError:
                  print (os.path.join(folders,filename)+",NIL,NIL,"+mail_id[0])
              elif len(mail_id)==0:
                try:
                  print (os.path.join(folders,filename)+',NIL,NIL,NIL')
                except AttributeError:
                  print (os.path.join(folders,filename)+',NIL,NIL,NIL')

        elif (lic_name!=None) & (len(author_name)==0):
          if len(mail_id)!=0:
            try:
             print (os.path.join(folders,filename)+','+lic_name.group(0)+",NIL,"+mail_id[0])
            except AttributeError:
             print (os.path.join(folders,filename)+","+lic_name+",NIL,"+mail_id[0])
          elif len(mail_id)==0:
           try:
             print (os.path.join(folders,filename)+','+lic_name.group(0)+',NIL,NIL')
           except AttributeError:
             print (os.path.join(folders,filename)+','+lic_name+',NIL,NIL')
